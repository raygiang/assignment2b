﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2a.usercontrols
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string Code
        {
            get { return (string)ViewState["Code"]; }
            set { ViewState["Code"] = value; }
        }

        public string Owner
        {
            get { return (string)ViewState["Owner"]; }
            set { ViewState["Owner"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> codeToPrint;

            if (Code == "webProg" && Owner == "teacher")
            {
                codeToPrint = new List<string>(new string[]{
                    "var userInput = prompt(\"Do you prefer red, green, or blue?\", \"Please say blue.\")",
                    "",
                    "if(userInput === \"\" || userInput === null || userInput === \"Please say blue.\"){",
                    "~console.log(\"You need to enter a colour!\");",
                    "}",
                    "else{",
                    "",
                    "~switch (userInput.toLowerCase().trim())",
                    "~{",
                    "~~case \"red\":",
                        "~~~console.log(\"Red is rad\");",
                        "~~~break;",
                    "~~case \"green\":",
                        "~~~console.log(\"Green is great!\");",
                        "~~~break;",
                    "~~case \"blue\":",
                        "~~~console.log(\"Blue is best!\");",
                        "~~~break;",
                    "~~default:",
                        "~~~console.log(\"Not one of the big 3\");",
                        "~~~break;",
                    "~}",
                    "}"
                });
            }
            else if (Code == "webProg" && Owner == "me")
            {
                codeToPrint = new List<string>(new string[]{
                    "var userChoice;",
                    "var enterNumError = \"Please enter a number between 1 and 10!\";",
                    "var validNumFlag = false;",
                    "",
                    "while (validNumFlag === false){",
                    "~userChoice = parseInt(prompt(\"Which top 10 book would you like?\",",
                    "~~\"Pick a number: 1-10\"));",
                    "~if (isNaN(userChoice) || userChoice < 1 || userChoice > 10){",
                    "~~alert(enterNumError);",
                    "~}",
                    "~else{",
                    "~~validNumFlag = true;",
                    "~}",
                    "}"
                });
            }
            else if (Code == "webApps" && Owner == "teacher")
            {
                codeToPrint = new List<string>(new string[]{
                    "switch (myDogBreed)",
                    "{",
                    "~case 1:",
                    "~~Chihuahua mychihuahua = new Chihuahua(myDogName, myDogAge);",
                    "~~dogRes.InnerHtml = mychihuahua.ChihuahuaTrick(command);",
                    "~~break;",
                    "~case 2:",
                    "~~Bulldog mybulldog = new Bulldog(myDogName, myDogAge);",
                    "~~dogRes.InnerHtml += mybulldog.BulldogTrick(command);",
                    "~~break;",
                    "~case 3:",
                    "~~Shepherd myshepherd = new Shepherd(myDogName, myDogAge);",
                    "~~dogRes.InnerHtml = myshepherd.ShepherdTrick(command);",
                    "~~break;",
                    "~case 4:",
                    "~~Rotweiler myrotweiler = new Rotweiler(myDogName, myDogAge);",
                    "~~dogRes.InnerHtml = myrotweiler.RotweilerTrick(command);",
                    "~~break;",
                    "}"
                });
            }
            else if (Code == "webApps" && Owner == "me")
            {
                codeToPrint = new List<string>(new string[]{
                    "public double CalculatePrice()",
                    "{",
                    "~double price = 10.00;",
                    "",
                    "~price += 5.00 * appointment.ServicesRequired.Count;",
                    "~if (appointment.GetDog)",
                    "~{",
                    "~~price += 2.50;",
                    "~}",
                    "~if (appointment.ReturnDog)",
                    "~{",
                    "~~price += 2.50;",
                    "~}",
                    "",
                    "~price += 1.00 * (appointment.ReturnDate.Hour - ",
                    "~~appointment.ReceiveDate.Hour);",
                    "",
                    "~return price;",
                    "}"
                });
            }
            else if (Code == "database" && Owner == "teacher")
            {
                codeToPrint = new List<string>(new string[]{
                    "SELECT primary_class, ROUND(AVG(intelligence), 3) as smarts",
                    "FROM characters",
                    "GROUP BY primary_class",
                    "ORDER BY smarts DESC;"
                });
            }
            else if (Code == "database" && Owner == "me")
            {
                codeToPrint = new List<string>(new string[]{
                    "SELECT premisetype",
                    "FROM autotheft",
                    "GROUP BY premisetype",
                    "HAVING COUNT(*) > 500;"
                });
            }
            else
            {
                codeToPrint = new List<string>();
            }

            // The following code for forming the DataTable follows the example from class

            DataTable codeTable = new DataTable();

            DataColumn indexColumn = new DataColumn();
            DataColumn codeColumn = new DataColumn();

            indexColumn.ColumnName = "Line";
            indexColumn.DataType = System.Type.GetType("System.Int32");
            codeTable.Columns.Add(indexColumn);

            codeColumn.ColumnName = "Code";
            codeColumn.DataType = System.Type.GetType("System.String");
            codeTable.Columns.Add(codeColumn);

            DataRow codeRow;

            int i = 0;
            foreach (string codeLine in codeToPrint)
            {
                codeRow = codeTable.NewRow();
                codeRow[indexColumn.ColumnName] = i;
                string formattedCode = System.Net.WebUtility.HtmlEncode(codeLine);
                formattedCode = formattedCode.Replace("~", "&nbsp;&nbsp;&nbsp;&nbsp");
                codeRow[codeColumn.ColumnName] = formattedCode;

                codeTable.Rows.Add(codeRow);
                i++;
            }

            DataView codeView = new DataView(codeTable);
            myCodeBox.DataSource = codeView;
            myCodeBox.DataBind();
        }
    }
}